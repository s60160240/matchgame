//
//  ContentView.swift
//  Reswift
//
//  Created by student on 3/9/20.
//  Copyright © 2020 student. All rights reserved.
//

import SwiftUI


struct ContentView: View {
    @State var imagePath1 = "TU"
    @State var imagePath2 = "BUU"
    @State var imagePath3 = "CMU"
    @State var imagePath4 = "NPU"
    @State var imagePath5 = "SUT"
    @State var imagePath6 = "CU"
    @State var imagePath7 = "TU"
    @State var imagePath8 = "BUU"
    @State var imagePath9 = "CMU"
    @State var imagePath10 = "SUT"
    @State var imagePath11 = "CU"
    @State var imagePath12 = "NPU"
    @State var count = 0
    
    @State var firstClick = ""
    @State var seccondClick = ""
    @State var firstIndexClick = 0
    @State var seccondIndexClick = 0
    
    func clickImage(imagePath:String,index:Int){
        if(imagePath == ""){
            
        }
        else if(self.firstClick == "" ){
            self.firstClick = imagePath
            self.firstIndexClick = index
        }else if(self.firstIndexClick == index){
            
        }else{
            self.seccondClick = imagePath
            self.seccondIndexClick = index
            
            if(self.firstClick == self.seccondClick){
                if(self.firstIndexClick == 1){
                    self.imagePath1 = ""
                }else if(self.firstIndexClick == 2){
                    self.imagePath2 = ""
                }else if(self.firstIndexClick == 3){
                    self.imagePath3 = ""
                }else if(self.firstIndexClick == 4){
                    self.imagePath4 = ""
                }else if(self.firstIndexClick == 5){
                    self.imagePath5 = ""
                }else if(self.firstIndexClick == 6){
                    self.imagePath6 = ""
                }else if(self.firstIndexClick == 7){
                    self.imagePath7 = ""
                }else if(self.firstIndexClick == 8){
                    self.imagePath8 = ""
                }else if(self.firstIndexClick == 9){
                    self.imagePath9 = ""
                }else if(self.firstIndexClick == 10){
                    self.imagePath10 = ""
                }else if(self.firstIndexClick == 11){
                    self.imagePath11 = ""
                }else if(self.firstIndexClick == 12){
                    self.imagePath12 = ""
                }
                
                
                if(self.seccondIndexClick == 1){
                    self.imagePath1 = ""
                }else if(self.seccondIndexClick == 2){
                    self.imagePath2 = ""
                }else if(self.seccondIndexClick == 3){
                    self.imagePath3 = ""
                }else if(self.seccondIndexClick == 4){
                    self.imagePath4 = ""
                }else if(self.seccondIndexClick == 5){
                    self.imagePath5 = ""
                }else if(self.seccondIndexClick == 6){
                    self.imagePath6 = ""
                }else if(self.seccondIndexClick == 7){
                    self.imagePath7 = ""
                }else if(self.seccondIndexClick == 8){
                    self.imagePath8 = ""
                }else if(self.seccondIndexClick == 9){
                    self.imagePath9 = ""
                }else if(self.seccondIndexClick == 10){
                    self.imagePath10 = ""
                }else if(self.seccondIndexClick == 11){
                    self.imagePath11 = ""
                }else if(self.seccondIndexClick == 12){
                    self.imagePath12 = ""
                }
//                self.imagePath7 = ""
                count += 1
            }
            firstClick = ""
            seccondClick = ""
            firstIndexClick = 0
            seccondIndexClick = 0
        }
    }
    func resetGame(){
        imagePath1 = "TU"
        imagePath2 = "BUU"
        imagePath3 = "CMU"
        imagePath4 = "NPU"
        imagePath5 = "SUT"
        imagePath6 = "CU"
        imagePath7 = "TU"
        imagePath8 = "BUU"
        imagePath9 = "CMU"
        imagePath10 = "SUT"
        imagePath11 = "CU"
        imagePath12 = "NPU"
        count = 0
        
        firstClick = ""
        seccondClick = ""
        firstIndexClick = 0
        seccondIndexClick = 0
        
    }
    
    
    var body: some View {
        VStack{
            Text("คุณเลือก \(firstClick) ")
            Text("game").font(.title)
            HStack{
                Button(action: { self.clickImage(imagePath:self.imagePath1, index: 1) }) {
                    Image(imagePath1).resizable().renderingMode(.original).modifier(PrimaryImage())
                }
                Button(action:{ self.clickImage(imagePath: self.imagePath2, index: 2) }) {
                    Image(imagePath2).resizable().renderingMode(.original).modifier(PrimaryImage())
                }
                Button(action: { self.clickImage(imagePath: self.imagePath3, index: 3) }) {
                    Image(imagePath3).resizable().renderingMode(.original).modifier(PrimaryImage())
                }
                
                
            }

            HStack{
                Button(action: { self.clickImage(imagePath: self.imagePath4, index: 4) }) {
                    Image(imagePath4).resizable().renderingMode(.original).modifier(PrimaryImage())
                }
                Button(action: { self.clickImage(imagePath: self.imagePath5, index: 5) }) {
                    Image(imagePath5).resizable().renderingMode(.original).modifier(PrimaryImage())
                }
                Button(action: { self.clickImage(imagePath: self.imagePath6, index: 6) }) {
                    Image(imagePath6).resizable().renderingMode(.original).modifier(PrimaryImage())
                }
                
            }
            HStack{
                Button(action:{ self.clickImage(imagePath: self.imagePath7, index: 7) }) {
                    Image(imagePath7).resizable().renderingMode(.original).modifier(PrimaryImage())
                }
                Button(action: { self.clickImage(imagePath: self.imagePath8, index: 8) }) {
                    Image(imagePath8).resizable().renderingMode(.original).modifier(PrimaryImage())
                }
                Button(action: { self.clickImage(imagePath: self.imagePath9, index: 9) }) {
                    Image(imagePath9).resizable().renderingMode(.original).modifier(PrimaryImage())
                }
                
                
            }

            HStack{
                Button(action: { self.clickImage(imagePath: self.imagePath10, index: 10) }) {
                    Image(imagePath10).resizable().renderingMode(.original).modifier(PrimaryImage())
                }
                Button(action: { self.clickImage(imagePath: self.imagePath11, index: 11) }) {
                    Image(imagePath11).resizable().renderingMode(.original).modifier(PrimaryImage())
                }
                Button(action: { self.clickImage(imagePath: self.imagePath12, index: 12) }) {
                    Image(imagePath12).resizable().renderingMode(.original).modifier(PrimaryImage())
                }
                
            }
            if(count == 6) {
                Button(action: {self.resetGame()}) {
                    Text("เริ่มใหม่").font(.title)
                }
            }

        }
    }
    struct PrimaryImage:ViewModifier {
    func body(content: Content) -> some View{
        content
        .frame(width:120,height: 120)
    }
    
  }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
