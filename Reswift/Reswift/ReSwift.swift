//
//  ReSwift.swift
//  Reswift
//
//  Created by student on 3/9/20.
//  Copyright © 2020 student. All rights reserved.
//

import SwiftUI

struct AppState: StateType {

}

func appReducer(action: Action, state: AppState?) -> AppState {
  return AppState()
}

var store = Store<AppState>(reducer: appReducer, state: nil)


struct ReSwift: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct ReSwift_Previews: PreviewProvider {
    static var previews: some View {
        ReSwift()
    }
}
